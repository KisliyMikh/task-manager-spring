package ru.kiselev.tm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kiselev.tm.models.Task;

@Repository
public interface TaskRepository extends CrudRepository <Task, Long> {
}
