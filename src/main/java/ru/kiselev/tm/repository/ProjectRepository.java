package ru.kiselev.tm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kiselev.tm.models.Project;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

}
