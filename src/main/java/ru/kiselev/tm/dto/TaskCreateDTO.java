package ru.kiselev.tm.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskCreateDTO {

    public TaskCreateDTO(Long projectId){
        this.projectId = projectId;
    }

    private long projectId;

    private String name;

    private String description;

    @DateTimeFormat(style = "dd-MM-yyyy")
    private Date dateStart;
    @DateTimeFormat(style = "dd-MM-yyyy")
    private Date dateEnd;
}
