package ru.kiselev.tm.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.kiselev.tm.dto.TaskCreateDTO;
import ru.kiselev.tm.models.Project;
import ru.kiselev.tm.models.Task;
import ru.kiselev.tm.repository.ProjectRepository;

@Component
@RequiredArgsConstructor
public class TaskMapper {

    private final ProjectRepository projectRepository;

    public Task toEntity(TaskCreateDTO task) {
        Project project = projectRepository.findById(task.getProjectId()).
                orElseThrow(() -> new RuntimeException("Project not found"));
        return Task.builder()
                .project(project)
                .name(task.getName())
                .description(task.getDescription())
                .dateStart(task.getDateStart())
                .dateEnd(task.getDateEnd())
                .build();
    }
}
