package ru.kiselev.tm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kiselev.tm.models.Project;
import ru.kiselev.tm.service.ProjectService;

import javax.validation.Valid;

@Controller
@RequestMapping("/project")
public class ProjectController {

    private ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/new")
    public String newProject(Model model) {
        model.addAttribute("project", new Project());
        return "project/new";
    }

    @PostMapping
    public String create(@ModelAttribute("project") @Valid Project project,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "project/new";
        }
        projectService.create(project);
        return "redirect:/project";
    }

    @PatchMapping("/")
    public String update(@ModelAttribute("project") @Valid Project project, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "project/edit";
        }
        projectService.update(project);
        return "redirect:/project";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") long id, Model model) {
        model.addAttribute("project", projectService.show(id));
        return "project/show";
    }

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("project", projectService.getProject());
        return "project/index";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable("id") long id, Model model) {
        model.addAttribute("project", projectService.show(id));
        return "project/edit";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") long id) {
        projectService.delete(id);
        return "redirect:/project";
    }
}
