package ru.kiselev.tm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kiselev.tm.dto.TaskCreateDTO;
import ru.kiselev.tm.models.Task;
import ru.kiselev.tm.service.TaskService;

import javax.validation.Valid;

@Controller
@RequestMapping("/project/{projectId}/task")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/new")
    public String newTask(@PathVariable("projectId") long projectID, Model model) {
        model.addAttribute("task", new TaskCreateDTO(projectID));
        return "task/new";
    }

    @PostMapping
    public String create(@ModelAttribute("task") @Valid TaskCreateDTO task,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "task/new";
        }
        taskService.create(task);
        return "redirect:/project/{projectId}/edit";
    }

    @PatchMapping("/")
    public String update(@ModelAttribute("task") @Valid Task task,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "task/edit";
        }
        taskService.update(task);
        return "redirect:/task";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") long id, Model model) {
        model.addAttribute("task", taskService.show(id));
        return "task/show";
    }

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("task", taskService.getTask());
        return "task/index";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable("id") long id, Model model) {
        model.addAttribute("task", taskService.show(id));
        return "task/edit";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") long id) {
        taskService.delete(id);
        return "redirect:/task";
    }
}
