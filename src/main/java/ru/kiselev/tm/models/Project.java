package ru.kiselev.tm.models;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "PROJECT")
public class Project {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty(message = "Name should not be empty")
    @Size(min = 2, max = 15, message = "Name should be between 2 and 30 characters")
    private String name;

    private String description;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date dateStart;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date dateEnd;

    @OneToMany(mappedBy = "project")
    private List<Task> taskList;
}
