package ru.kiselev.tm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kiselev.tm.models.Project;
import ru.kiselev.tm.repository.ProjectRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Component
public class ProjectService {

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Autowired
    private final ProjectRepository projectRepository;



    public void create(Project project) {
        projectRepository.save(project);
    }

    public List<Project> getProject() {
        Iterable<Project> all = projectRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false)
                .collect(Collectors.toList());
    }

    public Project show(long id) {
        return projectRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Not found project by id: " + id));
    }

    public void update(Project updateProject) {
        long id = updateProject.getId();
        if (projectRepository.existsById(id)) {
            projectRepository.save(updateProject);
        } else {
            throw new RuntimeException("Not found project by id: " + id);
        }
    }

    public void delete(long id) {
        projectRepository.deleteById(id);
    }
}
