package ru.kiselev.tm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.kiselev.tm.dto.TaskCreateDTO;
import ru.kiselev.tm.mapper.TaskMapper;
import ru.kiselev.tm.models.Task;
import ru.kiselev.tm.repository.TaskRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
@Component
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;


    public void create(TaskCreateDTO task) {
        Task newTask = taskMapper.toEntity(task);
        taskRepository.save(newTask);
    }

    public List<Task> getTask() {
        Iterable<Task> all = taskRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false)
                .collect(Collectors.toList());
    }

    public Task show(long id) {
        return taskRepository.findById(id).orElseThrow(() -> new RuntimeException("Not found task by id " + id));
    }

    public void update(Task taskUpdate) {
        long id = taskUpdate.getId();
        if (taskRepository.existsById(id)) {
            taskRepository.save(taskUpdate);
        } else {
            throw new RuntimeException("Not found task by id" + id);
        }
    }

    public void delete(long id) {
        taskRepository.deleteById(id);
    }
}
