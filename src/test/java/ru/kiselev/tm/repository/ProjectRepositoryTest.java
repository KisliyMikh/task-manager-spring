package ru.kiselev.tm.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kiselev.tm.models.Project;

import static org.junit.jupiter.api.Assertions.*;

class ProjectRepositoryTest extends AbstractIntegrationTest{

    @Autowired
    private ProjectRepository projectRepository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void test(){
        Project project = new Project();
        project.setName("12");
        projectRepository.save(project);

        Project project1 = new Project();
        project1.setName("12");
        projectRepository.save(project1);

        System.out.println(project);
        System.out.println(project1);
    }

}